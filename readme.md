# Trabajo Práctico 5

## Arboles

1. 
    Se tiene la siguiente implementación de un árbol binario:
    ```java
    public class BinaryTree<T> {
     
       private T value;
       private BinaryTree<T> left;
       private BinaryTree<T> right;
        
       public BinaryTree(T value) {
          this(value, null, null);
       }
        
       public BinaryTree(T value, BinaryTree<T> left, BinaryTree<T> right) {
          this.value = value;
          this.left = left;
          this.right = right;
       }
        
       public T getValue() {
          return value;
       }
        
       public BinaryTree<T> getLeft() {
          return left;
       }
        
       public BinaryTree<T> getRight() {
          return right;
       }
    }
    ```
    Implementar métodos que resuelvan las siguientes operaciones:
    - Calcular la altura del árbol (el árbol vacío tiene altura -1).
    - Calcular la cantidad de nodos.
    - Calcular cuántas veces aparece un determinado valor en el árbol.
    - Calcular el ancho de un determinado nivel (cantidad real de nodos en dicho nivel).
    - Buscar el mayor elemento.

2. Implementar un algoritmo que dado un árbol binario de tipo T y una función f: T → S, construya un nuevo árbol de tipo S con la misma estructura que el original, resultado de aplicarle la función a cada uno de los nodos. 

3. Implementar un método que determine si dos árboles binarios son espejados sólo en cuanto a su estructura. Ejemplo: Los siguientes árboles son espejados.

    ![Arboles espejados](https://i.imgur.com/WQzgqnW.png)

4. Implementar un algoritmo que dado un arreglo de números enteros ordenados en forma ascendente y sin elementos repetidos construya un árbol binario de búsqueda perfectamente balanceado que contenga todos los elementos del vector. No se debe utilizar ningún algoritmo de balanceo de árboles.

5. El siguiente código imprime los nodos de un árbol binario por niveles (primero la raíz, luego los nodos hijos, luego los nietos, y así sucesivamente). Analizar su complejidad temporal y proponer una solución que sea O(N), siendo N la cantidad total de nodos.
    ```java
    public static <T> void printNodes(BinaryTree<T> tree) {
       int level = 0;
       while (printNodesByLevel(tree, level) > 0) {
          level++;
       }
    }
        
    private static <T> int printNodesByLevel(BinaryTree<T> tree, int level) {
       if (tree == null) {
          return 0;
       }
       if (level == 0) {
          System.out.println(tree.getValue());
          return 1;
       }
       return printNodesByLevel(tree.getLeft(), level - 1) +
                       printNodesByLevel(tree.getRight(), level - 1);
    }
    ```
6. Implementar un algoritmo que determine si un árbol binario cumple las condiciones de árbol binario de búsqueda. 

7. Implementar un algoritmo que determine si un árbol binario cumple las condiciones de árbol AVL. 

8. Implementar un árbol binario de búsqueda, que responda a la siguiente interfaz:

    ```java
    public interface BinarySearchTree<T> {
     
       /**
        * Agrega una clave al árbol. 
        * Si ya existe, no hace nada y el árbol no se modifica.
        */
       public void add(T key);
        
       /**
        * Elimina una clave del árbol. 
        * Si no existe, no hace nada y el árbol no se modifica.
        */
       public void remove(T key);
        
       /**
        * Determina si el árbol contiene o no una clave.
        */
       public boolean contains(T key);
        
       /**
        * Retorna la cantidad de claves almacenadas.
        */
       public int size();
    }
    ```

9. Agregarle a la interfaz del ejercicio anterior métodos para resolver las siguientes operaciones:
    - Dado un valor obtener el nivel que ocupa en el árbol (si el elemento no existe retorna -1).
    - Determinar cuántas hojas tiene.
    - Buscar el mayor elemento.
    - Imprimir todos los antecesores de un determinado nodo.
    - Imprimir todos los descendientes de un determinado nodo.

10. Agregarle al árbol binario de búsqueda los métodos equals y hashCode.

11. Agregarle al árbol binario de búsqueda métodos que permitan construir iteradores para obtener los nodos según recorridos preorder, inorder y postorder.  
 
12. Implementar un algoritmo que permita realizar una búsqueda por rango en un árbol binario de búsqueda. Es decir, dado un valor máximo y un mínimo debe retornar un conjunto con todos aquellos valores que pertenecen al intervalo.  

13. Modificar la implementación del árbol binario de búsqueda para que las inserciones y borrados mantengan el árbol balanceado de acuerdo al algoritmo AVL.

14. Implementar una clase que permita representar un árbol de expresiones. Escribir un programa que dada una expresión en notación prefija construya su correspondiente árbol, y luego evalúe dicha expresión.
Ejemplo: Para la expresión * 3 + 4 5 se debería construir el siguiente árbol, y evaluarse como 27.

    ![Arbol ejemplo](https://i.imgur.com/O8GSOvu.png)

15. Implementar un algoritmo que construya un árbol de expresiones a partir de una expresión en notación posfija.

