package com.eda.itba;

import java.util.Comparator;
import java.util.Iterator;

public class BST<T> implements BinarySearchTree<T>{

    public BinaryTree<T> root;
    public Comparator<T> cmp;

    public BST(Comparator<T> cmp) {
        this.cmp = cmp;
    }

    public BST(BinaryTree<T> root, Comparator<T> cmp) {
        this.root = root;
        this.cmp = cmp;
    }

    @Override
    public void add(T key) {
        if (root == null) {
            root = new BinaryTree<>(key);
            return;
        }
        root.add(key, cmp);
    }

    /**
     * Elimina una clave del árbol.
     * Si no existe, no hace nada y el árbol no se modifica.
     */
    @Override
    public void remove(T key){
        if (root == null) return;
        root = root.remove(key, cmp);
    }

    /**
     * Determina si el árbol contiene o no una clave.
     */
    @Override
    public boolean contains(T key){
        if (root == null) return false;
        return root.contains(key, cmp);
    }

    /**
     * Retorna la cantidad de claves almacenadas.
     */
    @Override
    public int size() {
        return BinaryTree.size(root);
    }

    @Override
    public T max() {
        if (root == null) return null;
        return root.rightMost().getValue();
    }

    public T min() {
        if (root == null) return null;
        return root.leftMost().getValue();
    }

    @Override
    public void printDescendants(T key) {
        BinaryTree<T> descendantsOf = findTree(key);
        if (descendantsOf != null)  {
            BinaryTree.print(descendantsOf);
        }
    }

    private BinaryTree<T> findTree(T key) {
        if (root == null) return null;
        return root.findTree(key,cmp);
    }

    @Override
    public void printParents(T key) {
        if (root == null) return;
        if (!contains(key)) return;
        root.printUntil(key, cmp);
    }

    @Override
    public int leavesCount() {
        if (root == null) return -1;
        return root.leavesCount();
    }

    @Override
    public int findLevel(T key) {
        if (root == null) return -1;
        return root.findLevel(key, cmp,0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BST<?> bst = (BST<?>) o;
//        if (!cmp.equals(bst.cmp)) return false;  Esta linea deberia estar, pero el equals entre lambdas me da false
        if (root == null && bst.root == null) return true;
        if (root == null) return false;
        if (bst.root == null) return false;
        return root.equals(bst.root);
    }

    @Override
    public int hashCode() {
        int hashCode = 83;
        if (root == null) return hashCode;
        hashCode += 89 * root.hashCode();
        return hashCode;
    }

    public Iterator<T> preorderIterator() {
        return new PreorderIterator<>(root);
    }

    public Iterator<T> inorderIterator() {
        return new InorderIterator<>(root);
    }

    public PostorderIterator<T> postorderIterator() {
        return new PostorderIterator<>(root);
    }
}
