package com.eda.itba;

public interface BinarySearchTree<T> {
    /**
     * Agrega una clave al árbol.
     * Si ya existe, no hace nada y el árbol no se modifica.
     */
    public void add(T key);

    /**
     * Elimina una clave del árbol.
     * Si no existe, no hace nada y el árbol no se modifica.
     */
    public void remove(T key);

    /**
     * Determina si el árbol contiene o no una clave.
     */
    public boolean contains(T key);

    /**
     * Retorna la cantidad de claves almacenadas.
     */
    public int size();

    /**
     * Dado un valor obtener el nivel que ocupa en el árbol (si el elemento no existe retorna -1).
     */
    public int findLevel(T key);

    /**
     * Determinar cuántas hojas tiene.
     */
    public int leavesCount();

    /**
     * Buscar el mayor elemento.
     */
    public T max();

    /**
     * Imprimir todos los antecesores de un determinado nodo ( voy a suponer q se pasa la key como parametro?)
     */
    public void printParents(T key);

    /**
     * Imprimir todos los descendientes de un determinado nodo ( voy a suponer q se pasa la key como parametro?)
     */
    public void printDescendants(T key);
}
