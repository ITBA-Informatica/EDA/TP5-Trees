package com.eda.itba;

import java.util.Comparator;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Objects;
import java.util.function.Function;

public class BinaryTree<T> {
    int height;
    private T value;
    private BinaryTree<T> left;
    private BinaryTree<T> right;

    public BinaryTree(T value) {
        this(value, null, null);
    }

    public BinaryTree(T value, BinaryTree<T> left, BinaryTree<T> right) {
        if (value != null) {
            this.value = value;
            this.left = left;
            this.right = right;
        }
    }

    public T getValue() {
        return value;
    }

    public BinaryTree<T> getLeft() {
        return left;
    }

    public int height() {
        return BinaryTree.height(this);
    }

    public BinaryTree<T> getRight() {
        return right;
    }

    public static <T> int height(BinaryTree<T> bt) {
        if (bt == null || bt.getValue() == null) return -1;
        return 1 + Math.max(BinaryTree.height(bt.getLeft()), BinaryTree.height(bt.getRight()));
    }

    public static <T> int size(BinaryTree<T> bt) {
        if (bt == null || bt.getValue() == null) return 0;
        return 1 + size(bt.getLeft()) + size(bt.getRight());
    }

    public int size() {
        return BinaryTree.size(this);
    }

    public static <T> int countIfEquals(T elem, BinaryTree<T> bt) {
        if (bt == null || bt.getValue() == null) return 0;
        return countIfEquals(elem, bt.getLeft()) + countIfEquals(elem, bt.getRight()) + (elem.equals(bt.getValue()) ? 1 : 0);
    }

    public static <T> int width(int level, BinaryTree<T> bt) {
        if (bt == null || bt.getValue() == null) return 0;
        if (level == 0) return 1;
        return width(level - 1, bt.getLeft()) + width(level - 1, bt.getRight());
    }

    public static <T> T max(BinaryTree<T> bt, Comparator<T> cmp) {
        if (bt == null || bt.getValue() == null) return null;
        T max = null;
        T maxL = max(bt.getLeft(), cmp);
        T maxR = max(bt.getRight(), cmp);
        if (maxL != null || maxR != null) {
            if (maxL == null) {
                max = maxR;
            } else if (maxR == null) {
                max = maxL;
            } else {
                max = cmp.compare(maxL, maxR) > 0 ? maxL : maxR;
            }
        }

        if (max != null) {
            max = cmp.compare(max, bt.getValue()) > 0 ? max : bt.getValue();
        } else {
            max = bt.getValue();
        }
        return max;
    }

    public static <T, S> BinaryTree<S> map(BinaryTree<T> bt, Function<T, S> operation) {
        if (bt == null || bt.getValue() == null) return null;
        BinaryTree<S> leftTree = map(bt.getLeft(), operation);
        BinaryTree<S> rightTree = map(bt.getRight(), operation);
        S newVal = operation.apply(bt.getValue());
        return new BinaryTree<S>(newVal, leftTree, rightTree);
    }

    public static <T, S> boolean mirrored(BinaryTree<T> bt1, BinaryTree<S> bt2) {
        if ((bt1 == null) && (bt2 == null)) return true;
        if (bt1 == null || bt2 == null) return false;
        return mirrored(bt1.getLeft(), bt2.getRight()) && mirrored(bt1.getRight(), bt2.getLeft());
    }

    public static <T> void print(BinaryTree<T> bt) {

        Deque<BinaryTree<T>> queue = new LinkedList<>();
        if (bt != null) {
            queue.offer(bt);
            queue.offer(new BinaryTree<>(null));
        }

        while (!queue.isEmpty() && queue.peek().getValue() != null) {
            BinaryTree<T> next = queue.poll();
            System.out.print(next.getValue() + " ");
            if (next.getLeft() != null) {
                queue.offer(next.getLeft());
            }
            if (next.getRight() != null) {
                queue.offer(next.getRight());
            }

            if (queue.peek() != null && queue.peek().getValue() == null) {
                System.out.println();
                queue.poll();
                queue.offer(new BinaryTree<>(null));
            }
        }
    }

    public static BinaryTree<Integer> arrayToTree(int[] array) {
        return arrayToTreeRec(array, 0, array.length - 1);
    }

    public static BinaryTree<Integer> arrayToTreeRec(int[] array, int start, int end) {
        if (start == end) return new BinaryTree<>(array[start]);
        int middle = (start + end) / 2;
        BinaryTree<Integer> left = null;
        BinaryTree<Integer> right = null;
        if (middle != start) {
            left = arrayToTreeRec(array, (start), middle - 1);
        }
        if (middle != end) {
            right = arrayToTreeRec(array, (middle + 1), end);
        }
        return new BinaryTree<>(array[middle], left, right);
    }

    public static <T> boolean validateBST(BinaryTree<T> bt, Comparator<T> cmp) {
        if (bt == null) return true;
        boolean currLevelValid = true;
        if (bt.getLeft() != null && (cmp.compare(bt.getValue(), bt.getLeft().getValue())) < 0) currLevelValid = false;
        if (bt.getRight() != null && (cmp.compare(bt.getValue(), bt.getRight().getValue())) > 0) currLevelValid = false;
        return validateBST(bt.getLeft(), cmp) && validateBST(bt.getRight(), cmp) && currLevelValid;

    }

    public static boolean validAvl(BinaryTree bt) {
        int l = bt.getLeft().height();
        int r = bt.getRight().height();
        if (Math.abs(l - r) > 1) {
            return false;
        }
        return true;
    }


    /**
     * Estos metodos son para el BST y no tener q reescribir toodo lo demas
     */
    public void add(T elem, Comparator<T> cmp) {
//        this nunca es null xq esoty en un metodo de instancia
//        this.value no deberia poder ser null
        if (cmp.compare(value, elem) == 0) {
            return;
        } else if (cmp.compare(value, elem) > 0) {
//            El nuevo elemento es menor ( va a la izquierda)
            if (left == null) {
                left = new BinaryTree<>(elem);
            } else {
                left.add(elem, cmp);
            }
        } else {
            if (right == null) {
                right = new BinaryTree<>(elem);
            } else {
                right.add(elem, cmp);
            }
        }
    }

    public BinaryTree<T> remove(T key, Comparator<T> cmp) {
        if (cmp.compare(value, key) == 0) {
//            Tengo q sacar este nodo! A ver como lo hacemos
            if (left == null && right == null) return null;
            if (left == null) return right;
            if (right == null) return left;
            if (right.height() >= left.height()) {
                right.leftMost().left = left;
                return right;
            } else {
                left.rightMost().right = right;
                return left;
            }
        }
        if (cmp.compare(value, key) > 0 && left != null) {
            left = left.remove(key, cmp);
        }
        if (cmp.compare(value, key) < 0 && right != null) {
            right = right.remove(key, cmp);
        }
        return this;
    }


    public boolean contains(T key, Comparator<T> cmp) {
        int comp = cmp.compare(key, value);
        if (comp == 0) {
            return true;
        }
        if (comp > 0) {
            if (right == null) return false;
            return right.contains(key, cmp);
        } else {
            if (left == null) return false;
            return left.contains(key, cmp);
        }
    }

    public BinaryTree<T> rightMost() {
        if (right == null) return this;
        return right.rightMost();
    }

    public BinaryTree<T> leftMost() {
        if (left == null) return this;
        return left.leftMost();
    }

    public BinaryTree<T> findTree(T key, Comparator<T> cmp) {
        int comp = cmp.compare(key, value);
        if (comp == 0) {
            return this;
        }
        if (comp > 0) {
            if (right == null) return null;
            return right.findTree(key, cmp);
        } else {
            if (left == null) return null;
            return left.findTree(key, cmp);
        }
    }

    public void printUntil(T key, Comparator<T> cmp) {
        System.out.print(value.toString() + " ");
        int comp = cmp.compare(key, value);
        if (comp == 0) {
            return;
        }
        if (comp > 0) {
            if (right == null) return;
            right.printUntil(key, cmp);
        } else {
            if (left == null) return;
            left.printUntil(key, cmp);
        }
    }

    public int leavesCount() {
        if (left == null && right == null) return 1;
        int count = 0;
        if (left != null) {
            count += left.leavesCount();
        }
        if (right != null) {
            count += right.leavesCount();
        }
        return count;
    }

    public int findLevel(T key, Comparator<T> cmp, int currLvl) {
        int comp = cmp.compare(key, value);
        if (comp == 0) {
            return currLvl;
        }
        if (comp > 0) {
            if (right == null) return -1;
            return right.findLevel(key, cmp, currLvl+1);
        } else {
            if (left == null) return -1;
            return left.findLevel(key, cmp, currLvl+1);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BinaryTree<?> that = (BinaryTree<?>) o;
        if (!getValue().equals(that.getValue())) return false;
//        Left
        if (left == null && that.left != null) return false;
        if (that.left == null && left != null) return false;
        if ((that.left == null && left == null) ||left.equals(that.left)) {
            if (right == null && that.right!= null) return false;
            if (that.right == null && right != null) return false;
            return ((that.right == null && right == null) ||right.equals(that.right));
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {

        int hashCode = getValue().hashCode();
        if (left != null) {
            hashCode  += 97 * left.hashCode();
        }
        if (right != null) {
            hashCode += 101 * right.hashCode();
        }
        return hashCode;
    }
}