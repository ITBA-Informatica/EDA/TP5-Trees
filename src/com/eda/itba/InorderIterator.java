package com.eda.itba;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class InorderIterator<T> implements Iterator<T> {

    Deque<BinaryTree<T>> stack = new LinkedList<>();
    BinaryTree<T> current;

    public InorderIterator(BinaryTree<T> root) {
        if (root != null) {
            current = pushLefts(root);
        }
    }
    private BinaryTree<T> pushLefts(BinaryTree<T> tree) {
        while(tree.getLeft() != null) {
            stack.push(tree);
            tree = tree.getLeft();
        }
        return tree;
    }

    @Override
    public boolean hasNext() {
        return current != null;
    }

    @Override
    public T next() {
        T value = current.getValue();
        if (current.getRight() != null) {
            current = pushLefts(current.getRight());
        } else {
            if (!stack.isEmpty()) {
                current = stack.pop();
            } else {
                current = null;
            }
        }
        return value;
    }
}
