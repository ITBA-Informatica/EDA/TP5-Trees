package com.eda.itba;

import java.util.Comparator;
import java.util.Iterator;

public class Main {

    public static void main(String[] args) {
        BinaryTree<Integer> bt0 = new BinaryTree<>(null);
	    BinaryTree<Integer> bt1 = new BinaryTree<>(1);
        BinaryTree<Integer> bt5 = new BinaryTree<>(5);
        BinaryTree<Integer> bt2 = new BinaryTree<>(2,null, null);
        BinaryTree<Integer> bt3 = new BinaryTree<>(3, bt2,bt1);
        BinaryTree<Integer> bt4 = new BinaryTree<>(4,bt3,bt5);


//          4
//       3     5
//     2
//   1
//
//        BinaryTree.print(bt4);
//        System.out.println(BinaryTree.height(bt0));
//        System.out.println(BinaryTree.size(bt4));
//        System.out.println(BinaryTree.countIfEquals(5,bt3));
//        System.out.println(BinaryTree.width(0,bt0));
//        BinaryTree<Integer> mappedTree = BinaryTree.map(bt4,(v) -> v + 5);
//        System.out.println(BinaryTree.max(mappedTree, (a,b) -> a.compareTo(b)));
//        System.out.println(BinaryTree.size(mappedTree));

//        System.out.println(BinaryTree.mirrored(bt2,bt3));
//        int[] testArray= new int[]{1,2,3,4,5,6, 7, 8,9,10, 11, 123};
//        BinaryTree<Integer> testTree = BinaryTree.arrayToTree(testArray);
//        System.out.println(BinaryTree.height(testTree));
//        System.out.println(testTree.getValue());
//        BinaryTree.print(testTree);
//        System.out.println(BinaryTree.validateBST(testTree,(a,b) -> a.compareTo(b)));
//        System.out.println(BinaryTree.validAvl(testTree));
//        System.out.println(BinaryTree.max(testTree,(a,b) -> a.compareTo(b)));
        BST<Integer> testBst = new BST<>((a,b) -> a.compareTo(b));
        testBst.add(5);
        testBst.add(1);
        testBst.add(0);
        testBst.add(3);
        testBst.add(2);
        testBst.add(5);
        testBst.add(10);
        testBst.add(15);
        testBst.add(9);


        BST<Integer> testBst2 = new BST<>((a,b) -> a.compareTo(b));
        testBst2.add(5);
        testBst2.add(1);

        testBst2.add(3);
        testBst2.add(2);
        testBst2.add(5);
        testBst2.add(10);
        testBst2.add(15);
        testBst2.add(9);

        Iterator<Integer> it = testBst.postorderIterator();
        System.out.println(it.next());
        System.out.println(it.next());
        System.out.println(it.next());
        System.out.println(it.next());
        System.out.println(it.next());
        System.out.println(it.next());
        System.out.println(it.next());
        System.out.println(it.next());




        /*
        *
        *          5
        *     1          10
        *  0     3     9    15
        *      2
        *
        *
        */

//        0 1 2 3 5 9 10 15

        /*
        1: 5
        2: 1 10

        1: 1 5
        2: 0 3 10


         */

//        System.out.println(BinaryTree.validateBST(testBst.root, testBst.cmp));
//        BinaryTree.print(testBst.root);
//        System.out.println(BinaryTree.validateBST(testBst.root,testBst.cmp));
//        System.out.println(testBst.min());
//        System.out.println(testBst.contains(10));
//        System.out.println(testBst.size());
//        System.out.println(testBst.findLevel(0));
    //        testBst.printParents(15);
    }
}
