package com.eda.itba;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class PostorderIterator<T> implements Iterator<T> {

    Deque<BinaryTree<T>> stack = new LinkedList<>();
    BinaryTree<T> current;


    public PostorderIterator(BinaryTree<T> root) {
        if (root != null) {
            pushTilLeaf(root);
        }

    }

    private void pushTilLeaf(BinaryTree<T> tree) {
        if (tree == null) return;
        stack.push(tree);
        while(tree.getLeft() != null || tree.getRight() != null) {
            if (tree.getLeft() != null) {
                stack.push(tree.getLeft());
                tree = tree.getLeft();
            } else {
                stack.push(tree.getRight());
                tree = tree.getRight();
            }
        }
    }


    @Override
    public boolean hasNext() {
        return stack.isEmpty();
    }

    @Override
    public T next() {
        if (stack.isEmpty()) {
            throw new NoSuchElementException();
        }
        BinaryTree<T> nextTree = stack.pop();
        if (!stack.isEmpty() && stack.peek().getLeft().getValue().equals(nextTree.getValue())) {

            pushTilLeaf(stack.peek().getRight());
        }

        return nextTree.getValue();
    }
}
