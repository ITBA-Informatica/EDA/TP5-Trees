package com.eda.itba;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class PreorderIterator<T> implements Iterator<T> {
    Deque<BinaryTree<T>> stack = new LinkedList<>();

    public PreorderIterator(BinaryTree<T> root) {
        if (root != null) {
            stack.push(root);
        }
    }


    @Override
    public boolean hasNext() {
        return !stack.isEmpty();
    }

    @Override
    public T next() {
        BinaryTree<T> next = stack.pop();
        if (next.getRight() != null) stack.push(next.getRight());
        if (next.getLeft() != null) stack.push(next.getLeft());
        return next.getValue();
    }
}
